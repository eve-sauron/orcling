package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/antihax/goesi"
	"github.com/bradfitz/gomemcache/memcache"
	"github.com/gregjones/httpcache"
	httpmemcache "github.com/gregjones/httpcache/memcache"
	_ "github.com/lib/pq"
	"github.com/mediocregopher/radix.v2/redis"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

var (
	dbAddress  string
	dbPort     string
	dbDatabase string
	dbUser     string
	dbPass     string

	redisAddress string

	memcAddress string

	name string

	rqu        *redis.Client
	sqlDB      *sql.DB
	httpClient *http.Client
	eve        *goesi.APIClient
)

type zkill_record struct {
	KillmailID int       `json:"killmail_id"`
	ZKB        zkill_zkb `json:"zkb"`
}

type zkill_zkb struct {
	LocationID  int     `json:"locationId"`
	Hash        string  `json:"hash"`
	FittedValue float64 `json:"fittedValue"`
	TotalValue  float64 `json:"totalValue"`
	Points      int     `json:"points"`
	NPC         bool    `json:"npc"`
	Solo        bool    `json:"solo"`
	Awox        bool    `json:"awox"`
}

type esi_response struct {
	KillmailID   int       `json:"killmail_id"`
	KillmailTime time.Time `json:"killmail_time"`
	SystemID     int       `json:"solar_system_id"`

	Attackers []esi_attacker `json:"attackers"`
	Victim    esi_victim     `json:"victim"`
}

type esi_victim struct {
	AllianceID    int `json:"alliance_id"`
	CharacterID   int `json:"character_id"`
	CorporationID int `json:"corporation_id"`
	DamageTaken   int `json:"damage_taken"`
	ShipTypeID    int `json:"ship_type_id"`

	Position esi_position `json:"position"`
	Items    []esi_item   `json:"items"`
}

type esi_attacker struct {
	DamageDone     int     `json:"damage_done"`
	FactionID      int     `json:"faction_id"`
	AllianceID     int     `json:"alliance_id"`
	CharacterID    int     `json:"character_id"`
	CorporationID  int     `json:"corporation_id"`
	SecurityStatus float64 `json:"security_status"`
	ShipTypeID     float64 `json:"ship_type_id"`
	WeaponTypeId   int     `json:"weapon_type_id"`
	FinalBlow      bool    `json:"final_blow"`
}

type esi_item struct {
	Flag              int `json:"flag"`
	ItemTypeID        int `json:"item_type_id"`
	QuantityDropped   int `json:"quantity_dropped"`
	QuantityDestroyed int `json:"quantity_destroyed"`
	Singleton         int `json:"singleton"`
}

type esi_position struct {
	X float64 `json:"x"`
	Y float64 `json:"y"`
	Z float64 `json:"z"`
}

func main() {

	var err error

	getEnvVars()

	// Establish a connection to the Redis server listening on port 6379
	rqu, err = redis.Dial("tcp", redisAddress)
	if err != nil {
		log.Fatal(err)
	}
	defer rqu.Close()
	log.Println("Connected to Redis")

	// Establish connection to postgres
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s "+
		"password=%s dbname=%s sslmode=disable",
		dbAddress, dbPort, dbUser, dbPass, dbDatabase)
	sqlDB, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	defer sqlDB.Close()
	//confirm the connection
	err = sqlDB.Ping()
	if err != nil {
		panic(err)
	}
	log.Println("Connected to postgres")

	// Set up go esi with caching
	cache := memcache.New(memcAddress)
	transport := httpcache.NewTransport(httpmemcache.NewWithClient(cache))
	transport.Transport = &http.Transport{Proxy: http.ProxyFromEnvironment}
	httpClient = &http.Client{Transport: transport, Timeout: 10 * time.Second}

	eve = goesi.NewAPIClient(httpClient, "Sauron - Crypta Eve <crypta@crypta.tech>")

	// Main execution loop
	for {

		// Check redis for new id
		resp := rqu.Cmd("RPOPLPUSH", "intake", name)
		if resp.Err != nil {
			panic(err)
		}

		//Is there work on the queue, if so retrieve it
		resp = rqu.Cmd("RPOP", name)

		id, _ := resp.Int()

		if id != 0 {
			// Lets go to town

			log.Printf("Operating on id %d\n", id)

			// First check if hash exists....
			hash := getHashFromID(id)
			if len(hash) == 0 {
				// Apparently not, we can try get it from zkill
				log.Println("No hash in DB. THIS IS BAD!! Will attempt to rectify")

				hash = getHashFromZKill(id)

				if len(hash) > 0 {
					log.Println("Got a hash, inserting it!")
					insertStatement := fmt.Sprintf("INSERT INTO hashes (kill_id, hash) VALUES ('%s', '%s');",
						strconv.Itoa(id), hash)

					_, err = sqlDB.Exec(insertStatement)
					if err != nil {
						panic(err)
					}
				} else {
					log.Printf("Unable to get hash for kill:%d\n", id)
					continue
				}
			}

			// Check if zkill data is present, if not, fetch and populate it.
			zkillPresent := checkZKillPresent(id)
			if !zkillPresent {
				log.Println("Zkill data not stored, fetching")
				populateZKillData(id)
			}

			esiPresent := checkESIPresent(id)
			if !esiPresent {
				log.Println("ESI data not stored, fetching")
				populateESI(id, hash)
				//populateAxiom(id,hash)
			} else {
				//checkAxiomPresent(id)
				//populateAxiom(id,hash)
			}

			log.Println("Done")
		} else {
			time.Sleep(5 * time.Second)
		}
	}

}
func checkAxiomPresent(id int) {
	const selectStatement = "SELECT kill_id FROM zkill_data WHERE kill_id=$1"

	var valueTotal float64

	row := sqlDB.QueryRow(selectStatement, id)
	switch err := row.Scan(&valueTotal); err {
	case sql.ErrNoRows:
		return false
	case nil:
		return true
	default:
		panic(err)
	}
}
func populateESI(id int, hash string) {

	killmail, _, _ := eve.ESI.KillmailsApi.GetKillmailsKillmailIdKillmailHash(nil, hash, int32(id), nil)
	killmailJson, _ := killmail.MarshalJSON()
	killmailReader := bytes.NewReader(killmailJson)

	var response esi_response

	if err := json.NewDecoder(killmailReader).Decode(&response); err != nil {
		panic(err)
	}

	log.Printf("%#v\n", response)

	populateESIData(response)
	populateESIAttackers(response)
	populateESIItems(response)

}
func populateESIItems(esi esi_response) {
	for _, v := range esi.Victim.Items {
		const insertStatement = "INSERT INTO esi_items (kill_id, item_type_id, quantity_dropped, quantity_destroyed, flag, singleton) VALUES ($1, $2, $3, $4, $5, $6);"

		_, err := sqlDB.Exec(insertStatement,
			esi.KillmailID,
			v.ItemTypeID,
			v.QuantityDropped,
			v.QuantityDestroyed,
			v.Flag,
			v.Singleton)
		if err != nil {
			panic(err)
		}
	}
}
func populateESIAttackers(esi esi_response) {
	for _, v := range esi.Attackers {

		const insertStatement = "INSERT INTO esi_attackers (kill_id, char_id, damage_done, ship_id, weapon_type_id, security_status, final_blow) VALUES ($1, $2, $3, $4, $5, $6, $7);"

		_, err := sqlDB.Exec(insertStatement,
			esi.KillmailID,
			v.CharacterID,
			v.DamageDone,
			v.ShipTypeID,
			v.WeaponTypeId,
			v.SecurityStatus,
			v.FinalBlow)

		if err != nil {
			panic(err)
		}
	}
}
func populateESIData(esi esi_response) {
	// Just populating the esi_data and not its subtables

	const insertStatement = "INSERT INTO esi_data (kill_id, time, system_id, victim_char_id, victim_corp_id, victim_alliance_id, victim_ship_id, damage_taken, pos_x, pos_y, pos_z) " +
		"VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)"

	_, err := sqlDB.Exec(insertStatement,
		esi.KillmailID,
		esi.KillmailTime,
		esi.SystemID,
		esi.Victim.CharacterID,
		esi.Victim.CorporationID,
		esi.Victim.AllianceID,
		esi.Victim.ShipTypeID,
		esi.Victim.DamageTaken,
		esi.Victim.Position.X,
		esi.Victim.Position.Y,
		esi.Victim.Position.Z)

	if err != nil {
		panic(err)
	}

}
func checkESIPresent(id int) bool {
	const selectStatement = "SELECT kill_id FROM esi_data WHERE kill_id=$1"

	var valueTotal float64

	row := sqlDB.QueryRow(selectStatement, id)
	switch err := row.Scan(&valueTotal); err {
	case sql.ErrNoRows:
		return false
	case nil:
		return true
	default:
		panic(err)
	}
}

func getHashFromZKill(id int) (hash string) {
	//First fetch the zkill data from its api
	url := fmt.Sprintf("https://zkillboard.com/api/killID/%d/", id)

	log.Println(url)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal("NewRequest: ", err)
		return
	}
	req.Header.Set("User-Agent", "Sauron - Crypta Electrica")

	resp, err := httpClient.Do(req)
	if err != nil {
		log.Fatal("Do: ", err)
		return
	}

	defer resp.Body.Close()

	var response []zkill_record

	if err := json.NewDecoder(resp.Body).Decode(&response); err != nil {
		panic(err)
	}

	log.Println(response)

	if len(response) > 0 {
		return response[0].ZKB.Hash
	} else {
		return ""
	}

}

func populateZKillData(id int) () {
	//First fetch the zkill data from its api
	url := fmt.Sprintf("https://zkillboard.com/api/killID/%d/", id)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal("NewRequest: ", err)
		return
	}
	req.Header.Set("User-Agent", "Sauron - Crypta Electrica")

	resp, err := httpClient.Do(req)
	if err != nil {
		log.Fatal("Do: ", err)
		return
	}

	defer resp.Body.Close()

	var response []zkill_record

	if err := json.NewDecoder(resp.Body).Decode(&response); err != nil {
		panic(err)
	}

	killmail := response[0]

	insertSatement := fmt.Sprintf(
		"INSERT INTO zkill_data ( kill_id, location_id, value_fitted, value_total, points, npc, solo, awox) "+
			"VALUES (%v, %v, %v, %v, %v, %v, %v, %v)",
		killmail.KillmailID,
		killmail.ZKB.LocationID,
		killmail.ZKB.FittedValue,
		killmail.ZKB.TotalValue,
		killmail.ZKB.Points,
		killmail.ZKB.NPC,
		killmail.ZKB.Solo,
		killmail.ZKB.Awox)

	_, err = sqlDB.Exec(insertSatement)
	if err != nil {
		panic(err)
	}

}

func checkZKillPresent(id int) (present bool) {
	const selectStatement = "SELECT kill_id FROM zkill_data WHERE kill_id=$1"

	var valueTotal float64

	row := sqlDB.QueryRow(selectStatement, id)
	switch err := row.Scan(&valueTotal); err {
	case sql.ErrNoRows:
		return false
	case nil:
		return true
	default:
		panic(err)
	}
}

func getHashFromID(id int) (hash string) {
	// Check redis for new id

	const selectStatement = "SELECT hash FROM hashes where kill_id=$1;"

	row := sqlDB.QueryRow(selectStatement, id)
	switch err := row.Scan(&hash); err {
	case sql.ErrNoRows:
		log.Println(strconv.Itoa(id) + " has no hash available......")
		return ""
	case nil:
		return hash
	default:
		panic(err)
	}
}

func getEnvVars() () {
	dbAddress = os.Getenv("DB_ADDRESS")
	dbPort = os.Getenv("DB_PORT")
	dbDatabase = os.Getenv("DB_DATABASE")
	dbUser = os.Getenv("DB_USER")
	dbPass = os.Getenv("DB_PASSWORD")

	redisAddress = os.Getenv("RED_ADDRESS")

	memcAddress = os.Getenv("MEMCACHED_ADDRESS")

	name = os.Getenv("NAME")

	if len(dbAddress) == 0 ||
		len(dbPort) == 0 ||
		len(dbDatabase) == 0 ||
		len(dbUser) == 0 ||
		len(dbPass) == 0 ||
		len(redisAddress) == 0 ||
		len(name) == 0 ||
		len(memcAddress) == 0 {
		log.Println(os.Environ())
		panic("ENV VARS NOT SET!!!")
	}

}

